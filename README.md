# DockerScout 

DockerScout runs as a background service that aims to clean up old and least used saved docker images over time.

If you are finding that every few weeks your disk is full of old docker images you only used once or twice then this may be the tool for you.

DockerScout polices the disk space on the docker folder'd disk (usually `/var/lib/docker`).

# Quick Start

## Linux

```
$ pip install dockerscout
$ sudo python -m DockerScout --install
$ sudo systemctl start dockerscout
```

## Windows

Using an administrator mode command prompt:
```
pip install dockerscout
python -m DockerScout --install
```

# Configuration
On linux DockerScout will read `/etc/docker-scout.yml` and on windows `c:\\ProgramData\\.docker-scout\\docker-scout.yml`

If there is no configuration file, Scout will default to keeping 10Gb free on Linux and 30Gb free on windows.

```
scout:
  v1:
    # Aim to keep disk free space above 80g.
    min-disk-free: "80g"

```


